from django.shortcuts import render
from django.http import HttpResponse
from gpx_serializer import gpx_serializer

def index(request):
    seria = gpx_serializer()
    gpx = seria.gserialize()
    # gpx.reduce_points(400,75)
    points_list = gpx.tracks[0].segments[0]
    context = { 'points_list': points_list}
    return render(request, 'base.html', context)
