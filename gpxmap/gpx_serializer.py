from gpxmap.models import *
import gpxpy.parser as parser



class gpx_serializer:

    def __init__(self):
        intialized = True

    def simpleRead(self):
        gpx_file = open('/Users/oviera/PycharmProjects/untitled1/gpxmap/static/RK_gpx_2014-10-03_1001.gpx', 'r')
        gpx_parser = parser.GPXParser(gpx_file)

        gpx_parser.parse()

        gpx_file.close()

        gpx = gpx_parser.get_gpx()

        return gpx

    def createUser(self):
        wUser = User.objects.create(first_name = "Olivier", last_name="A")
        #wUser.save()

    def createRun(self, gpx):
        wUser = User.objects.get(id = 1)

        wRun = Run.objects.create(user = wUser, name = "No name", time = gpx.get_time_bounds().start_time)
        wRun.save()

        trackPoints = []
        for track in gpx.tracks:
            for (i, segment) in enumerate(track.segments):
                for (j, trkpt) in enumerate(segment.points):
                    trackPoints.append(Track_point(
                        run = wRun,
                        date = trkpt.time.date(),
                        time = trkpt.time.time(),
                        latitude = trkpt.latitude,
                        longitude = trkpt.longitude,
                        elevation = trkpt.elevation,
                        point = j,
                        segment = i))

        Track_point.objects.bulk_create(trackPoints)