from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

class Run(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=50)
    time = models.DateTimeField()

class Run_info(models.Model):
    run = models.ForeignKey(Run)
    total_distance = models.FloatField()
    total_elevation = models.FloatField()
    total_time = models.FloatField()

class Track_point(models.Model):
    run = models.ForeignKey(Run)
    date = models.DateField()
    time = models.TimeField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    elevation = models.FloatField()
    heartrate = models.IntegerField(null = True)
    elapsed_time = models.FloatField(null = True)
    track = models.IntegerField(null = True)
    point = models.IntegerField(null = True)
    segment = models.IntegerField(null = True)

